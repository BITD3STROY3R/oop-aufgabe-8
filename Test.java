/*
 * Objektorientierte Programmiertechniken ws2013 - Aufgabe 8 
 * =========================================================
 *
 *
 * Copyright 2013 Jannik Vierling e1226434@student.tuwien.ac.at,
 *		  Bernd Bogomolov e1225256@student.tuwien.ac.at,
 *		  Stefan Hanreich e1227486@student.tuwien.ac.at
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import java.lang.annotation.Annotation;
import java.lang.reflect.Array;
import java.lang.reflect.Method;
import java.util.Iterator;
import java.util.ArrayList;
import java.util.List;

@Author(name = "Jannik Vierling", matrnr = 1226434, 
		desc = "Implementierung der Klasse")
@Classes({Pumpkin.class, Hokaido.class, Butternut.class, Jackolantern.class,
	Author.class, Testing.class, Tests.class, Test.class, Classes.class})
@Tests({
	@Testing(pumpkins = {Hokaido.class}, 
	methods = {"reactToSun", "reactToRain", "reactToStorm", "reactToStorm"},
	instances = {1},
	collection = "[LPumpkin;"),
	
	@Testing(pumpkins = {Jackolantern.class},
	methods = {"reactToSun", "reactToRain", "reactToStorm"},
	instances = {1},
	collection = "java.util.ArrayList"),
	
	@Testing(pumpkins = {Butternut.class},
	methods = {"reactToSun", "reactToRain", "reactToStorm"},
	instances = {1},
	collection = "java.util.LinkedHashSet"),
	
	@Testing(pumpkins = {Hokaido.class, Jackolantern.class, Butternut.class},
	methods = {"reactToSun", "reactToRain", "reactToStorm"},
	instances = {1,1,1},
	collection = "java.util.LinkedList"),
	
	@Testing(pumpkins = {Hokaido.class, Jackolantern.class, Butternut.class},
	methods = {"reactToSun"},
	instances = {1,1,1},
	collection = "java.util.Vector"),
})
public class Test {
	
	/* Nachbedingung: Führt die in den Annotationen spezifizierten Testfälle
	 * aus, und gibt am ende eine zusammenfassung der Aufteilung der Arbeiten
	 * auf die Gruppenmitglieder aus.
	 */
	public static void main(String[] args) {
		Tests testAnnotation = Test.class.getAnnotation(Tests.class);
		Testing[] tests = testAnnotation.value();
	
		
		try {
			for (int i = 0; i < tests.length; i++) {
				System.out.println(makeHeader("Test " + (i+1)));
				doTest(tests[i]);
				System.out.println(makeSectionbar() + '\n');
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		/* Authoren und Aufteilugen anzeigen */
		Class<?>[] annotatedClasses
			= Test.class.getAnnotation(Classes.class).value();
		
		if (annotatedClasses != null) {
			for (int i = 0; i < annotatedClasses.length; i++) {
				List<Annotation> annotations 
					= findAnnotations(annotatedClasses[i]);
				
				ArrayList<Author> authorAnnotations = new ArrayList<Author>();
				for (Annotation a : annotations) {
					if (a instanceof Author) {
						authorAnnotations.add((Author) a);
					}
				}
				if (authorAnnotations.size() > 0) {
					System.out.println("Klasse "
							+ annotatedClasses[i].getName());
				}
				for (Author a : authorAnnotations) {
					System.out.println("\tName: " + a.name()
							+ ", MatrNr: " + a.matrnr()
							+ ", Aufgabe: " + a.desc());
				}
			}
		}
	}
	
	/* Vorbedingung: annotation != null
	 * Nachbedingung: Führt den in annotation spezifizierten Tesfall aus.
	 */
	private static void doTest(Testing annotation) 
			throws Exception {
		Class<?> collectionClass = Class.forName(annotation.collection());
		
		if (collectionClass.isArray()) {
			doTestArray(annotation, collectionClass);
		} else {
			doTestCollection(annotation);
		}
	}
	
	/* Vorbedingung: c != null
	 * Nachbedingung: Liefert alle Annotation der Klasse c, und die
	 * Annotationen aller Methoden der Klasse c.
	 */
	private static List<Annotation> findAnnotations(Class<?> c) {
		Method[] methods = c.getDeclaredMethods();
		ArrayList<Annotation> annotations = new ArrayList<Annotation>();
		
		Annotation[] ca = c.getAnnotations();
		for (int i = 0; i < ca.length; i++) {
			annotations.add(ca[i]);
		}
		for (int i = 0; i < methods.length; i++) {
			Annotation[] ma = methods[i].getDeclaredAnnotations();
			for (int j = 0; j < ma.length; j++) {
				annotations.add(ma[j]);
			}
		}
		return annotations;
	}
	
	/* Vorbedingung: annotation != null UND arrayClass != null
	 * UND arrayClass.isArray()
	 * Nachbedingung: Führt die in der annotation beschriebenen Tests anhand
	 * eines Arrays durch.
	 */
	private static void doTestArray(Testing annotation, Class<?> arrayClass)
		throws Exception {
		Object array = Array.newInstance(Object.class, 
				arrayLength(annotation));
		Class<?>[] pumpkins = annotation.pumpkins();
		int[] instances     = annotation.instances();
		String[] methods    = annotation.methods();
		
		/* Create pumpkin instances */
		int counter = 0;
		for (int i = 0; i < instances.length; i++) {
			for (int j = 0; j < instances[i]; j++) {
				Array.set(array, counter, pumpkins[i].newInstance());
				counter++;
			}
		}
		
		/* Print all */
		System.out.println("Before test\n");
		for (int i = 0; i < Array.getLength(array); i++) {
			System.out.println(Array.get(array, i));
		}
		System.out.println(makeSectionbar());
		
		/* Apply one method at a time */
		for (int i = 0; i < methods.length; i++) {
			for (int j = 0; j < Array.getLength(array); j++) {
				Object o = Array.get(array, j);
				Method m = o.getClass().getMethod(methods[i]);
				m.invoke(o);
			}
			/* Print all */
			System.out.println("Applied method " + methods[i] + "\n");
			for (int j = 0; j < Array.getLength(array); j++) {
				System.out.println(Array.get(array, j));
			}
			System.out.println(makeSectionbar());
		}
		
		
	}
	
	/* Vorbedingung: annotation != null, annotation.collection muss der Name
	 * einer Klasse sein welche das Interface Collection implementiert.
	 * Nachbedingung: Führt den in annotation spezifizierten Testfall anhand
	 * der in annotation.collection() spezifizierten Collection-Klasse aus.
	 */
	@Author(name="Jannik Vierling", matrnr=1226434,
			desc="Implementierung der Methode doTestCollection")
	private static void doTestCollection(Testing annotation)
			throws Exception {
		Object collection
			= Class.forName(annotation.collection()).newInstance();
		Method add      = collection.getClass().getMethod("add", Object.class);
		Method iterator = collection.getClass().getMethod("iterator");
		
		/* add instances */
		int[] instances = annotation.instances();
		for (int i = 0; i < instances.length; i++) {
			for (int j = 0; j < instances[i]; j++) {
				Object instance = annotation.pumpkins()[i].newInstance();
				add.invoke(collection, instance);
			}
		}
		
		/* excecute methods */
		String[] methods = annotation.methods();
		for (int i = 0; i < methods.length; i++) {
			Object it      = iterator.invoke(collection);
			Method hasNext = Iterator.class.getMethod("hasNext");
			Method next	   = Iterator.class.getMethod("next");
			while (hasNext.invoke(it).equals(true)) {
				Object o = next.invoke(it);
				Method m = o.getClass().getMethod(methods[i]);
				m.invoke(o);
			}
			System.out.println("Applied method " + methods[i] + "\n");
			printCollection(collection);
			System.out.println(makeSectionbar());
		}
	}
	
	/* Vorbedingung: o != null, o ist Untertyp von Collection
	 * Nachbedingung: Gibt die in o gespeicherten Objekte Zeilenweise am
	 * Bildschrim aus.
	 */
	private static void printCollection(Object o) throws Exception {
		Method iterator = o.getClass().getMethod("iterator");
		Object it       = iterator.invoke(o);
		Method hasNext  = Iterator.class.getMethod("hasNext");
		Method next     = Iterator.class.getMethod("next");
		
		while (hasNext.invoke(it).equals(true)) {
			Object n = next.invoke(it);
			System.out.println(n);
		}
	}
	
	/* Vorbedingung: annotation != null
	 * Nachbedingung: Liefert die Summe der Werte in annotation.instances().
	 */
	private static int arrayLength(Testing annotation) {
		int[] instances = annotation.instances();
		int sum = 0;
		
		for (int i = 0; i < instances.length; i++) {
			sum += instances[i];
		}
		
		return sum;
	}
	
	private static final int pageWidth   = 56;
	private static final char headerChar = '=';
	private static final char sectionChar = '-';
	private static final int headerWidth = 56;
	private static final int labelMargin = 4;
	
	/* Vorbedingung: n >= 0
	 * Nachbedingung: Liefert einen String bestehend aus der N-Fachen
	 * wiederholung des Zeichens c.
	 */
	private static String repeat(int n, char c) {
		String result = "";

		for (int i = 0; i < n; i++) {
			result += c;
		}

		return result;
	}

	/* Vorbedingung: label != null
	 * Nachbedingung: Liefert einen String der maximal headerWidth Zeichen
	 * lang ist. Der String beginnt und endet mit maximal labelMargin vielen
	 * Leerzeichen. Die Anzahl der nachfolgenden Leerzeichen ist immer gleich
	 * der der Fuehrenden.
	 */
	private static String makeLabel(String label) {
		if (label.length() <= headerWidth) {
			int i = 0;
			while (i < labelMargin && label.length() <= headerWidth - 2) {
				label = " " + label + " ";
				i++;
			}
		} else {
			label = label.substring(0, headerWidth);
		}

		return label;
	}

	/* Vorbedingung: label != null
	 * Nachbedingung: Liefert einen String welcher eine formatierte
	 * Ueberschrift darstellt. Label ist der Text der Ueberschrift.
	 */
	private static String makeHeader(String label) {
		String header = "";

		header += repeat(headerWidth, headerChar) + '\n';
		label = makeLabel(label);
		header += repeat((headerWidth - label.length()) / 2, headerChar);
		header += label;
		header += repeat((headerWidth - label.length()) / 2
				+ (headerWidth - label.length()) % 2, headerChar);
		header += '\n';
		header += repeat(headerWidth, headerChar);
		
		return header;
	}

	/* Nachbedingung: Liefert einen String welcher eine Abtrennungszeile
	 * darstellt. Diese Zeile hat eine Breite von genau pageWidth zeichen.
	 */
	private static String makeSectionbar() {
		return repeat(pageWidth, sectionChar);
	}

}