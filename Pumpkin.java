/*
 * Objektorientierte Programmiertechniken ws2013 - Aufgabe 8 
 * =========================================================
 *
 *
 * Copyright 2013 Jannik Vierling e1226434@student.tuwien.ac.at,
 *		  Bernd Bogomolov e1225256@student.tuwien.ac.at,
 *		  Stefan Hanreich e1227486@student.tuwien.ac.at
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


@Author(matrnr = 1226434, name = "Jannik Vierling", 
		desc = "Implementierung der Klasse")
public abstract class Pumpkin {
	
	/* Invariante: health >= 0 */
	private int health = 100;
	
	/* Invariante: weight >= 0 */
	private int weight = 0;
	
	/* Invariante: growth >= 0 */
	private int growth = 100;
	
	/* Vorbedingung : x >= 0
	 * Nachbedingung: Erniedrigt health um maximal x, health >= 0
	 */
	protected final void decreaseHealth(int x) {
		health -= x;
		health = health < 0 ? 0 : health;
	}
	
	/* Vorbedingung : x >= 0
	 * Nachbedingung: Erniedrigt growth um maximal x, growth >= 0
	 */
	protected final void decreaseGrowth(int x) {
		growth -= x;
		growth = growth < 0 ? 0 : growth;
	}
	
	/* Vorbedingung : x >= 0
	 * Nachbedingung: Erniedrigt weight um maximal x, weight >= 0
	 */
	protected final void decreaseWeight(int x) {
		weight -= x;
		weight = weight < 0 ? 0 : weight;
	}
	
	/* Vorbedingung : x >= 0
	 * Nachbedingung: Erhöht health um x.
	 */
	protected final void increaseHealth(int x) {
		health += x;
	}
	
	/* Vorbedingung : x >= 0
	 * Nachbedingung: Erhöht growth um x.
	 */
	protected final void increaseGrowth(int x) {
		growth += x;
	}
	
	/* Vorbedingung : x >= 0
	 * Nachbedingung: Erhöht weight um x.
	 */
	protected final void increaseWeight(int x) {
		weight += x;
	}
	
	/* Nachbedingung: Bildet in irgendeiner Weiße den Einfluß der
	 * sonne auf den Kürbis ab.
	 */
	public abstract void reactToSun();
	
	/* Nachbedingung: Bildet in irgendeiner Weiße den Einfluß des
	 * Regens auf den Kürbis ab.
	 */
	public abstract void reactToRain();
	
	/* Nachbedingung: Bildet in irgendeiner Weiße den Einfluß des
	 * Windes auf den Kürbis ab.
	 */
	public abstract void reactToStorm();

	/* Nachbedingung: Liefert eine Zeichenkette die den Namen der 
	 * Sorte des Kürbis enthält.
	 */
	protected abstract String getSort();
	
	/* Nachbedingung: Liefert eine Darstellung des Kürbis, als Zeichenkette
	 * in der Form:
	 * <Sorte>:   health = x, weight = y, growth = z
	 */
	@Override
	public String toString() {
		return getSort() + String.format(":\thealth = %3d"
				+ ", weight = %3d"
				+ ", growth = %3d", health, weight, growth);
	}
}
