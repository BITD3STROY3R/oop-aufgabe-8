/*
 * Objektorientierte Programmiertechniken ws2013 - Aufgabe 8 
 * =========================================================
 *
 *
 * Copyright 2013 Jannik Vierling e1226434@student.tuwien.ac.at,
 *		  Bernd Bogomolov e1225256@student.tuwien.ac.at,
 *		  Stefan Hanreich e1227486@student.tuwien.ac.at
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import java.lang.annotation.*;

/* Die Zusicherungen der Werte welche als Argumente für Annotationen übergeben
 * werden bzw. welche im Programm von den entsprechenden Methoden geliefert
 * werden wurden als Vorbedingungen gewählt, da der Benutzer für deren
 * Einhaltung verantwortlich ist.
 */

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface Testing {
	
	/* Vorbedingung: pumpkins().length == instances().length */
	public Class<? extends Pumpkin>[] pumpkins();
	
	/* Vorbedingung: für alle Strings in method gilt String != null, und
	 * existiert eine gleichnamige Methode in Pumpkin.
	 */
	public String[] methods();
	
	/* Vorbedingung: instances().length == pumpkins.length() */
	public int[] instances();
	
	/* Vorbedingung: collection() != null, collection() ist der Name einer
	 * Klasse welche Collection implementiert,
	 * oder collection().equals("[LPumpkin;").
	 */
	public String collection();
}