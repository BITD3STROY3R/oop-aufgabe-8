/*
 * Objektorientierte Programmiertechniken ws2013 - Aufgabe 8 
 * =========================================================
 *
 *
 * Copyright 2013 Jannik Vierling e1226434@student.tuwien.ac.at,
 *		  Bernd Bogomolov e1225256@student.tuwien.ac.at,
 *		  Stefan Hanreich e1227486@student.tuwien.ac.at
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


import java.lang.annotation.*;

@Author(name="Jannik Vierling", matrnr=1226434,
		desc="Implementierung der Annotation")
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE, ElementType.METHOD})
public @interface Author {
	
	/* Vorbedingung: Matrikelnummer des Autors. */
	public int matrnr();
	
	/* Vorbedingung: name() != null, Name des Autors. */
	public String name();
	
	/* Vorbedingung: desc() != null, Beschreibung des Beitrages des Autors. */
	public String desc();
}